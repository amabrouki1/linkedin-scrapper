import json
import math
import time

import pandas as pd
import tkinter as tk
import os
from DataGoogle import DataGoogle
from ProfileAnalyser import ProfileAnalyser




class Interface():

    def __init__(self):
        # Nombre de requete maximun
        self.new_root2 = None
        max_number_req = 20

        # Create a tkinter window
        self.root = tk.Tk()

        # create a scrollbar
        self.scrollbar = tk.Scrollbar(self.root)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        self.profiles = None
        self.mylist = tk.Listbox(self.root, yscrollcommand=self.scrollbar.set, width=100)

        # Liste pour stocker les objet de la window
        self.list_manipulable_object = {}

        # Entry list
        self.list_entry = [["Key words", 100, True, (), ''],
                           ["Must have", 100, True, (), ''],
                           ["Exclude", 100, True, (), ''],
                           ["email", 100, True, (), self.get_default_cred()['email']],
                           ["password", 100, False, (), self.get_default_cred()["password"]],
                           ["Requests amount - Do not exceed 20", 100, True,
                            (self.root.register(lambda s: s.isdigit() and 0 <= int(s) <= max_number_req), "%S"), ''],
                           ["Minimum experience years", 100, True, (), '0'],
                           ["Open to work? [Yes/No]", 100, True, (), ''],
                           ["Send a message", 100, True, (), ''],
                           ]


        # Creation des entry de la fenêtre
        [self.create_entry(attributes[0], attributes[1], attributes[2], attributes[3], attributes[4]) for attributes in
         self.list_entry]

        # pattern = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'

        # Create a button for headless/random or not
        self.alea = tk.BooleanVar()
        tk.Checkbutton(self.root, text='Ajouter un aléa à la recherche', variable=self.alea).pack()
        self.head = tk.BooleanVar()
        tk.Checkbutton(self.root, text='Afficher les profiles dans chrome', variable=self.head).pack()

        # Create a button to add data to the dataframe
        self.create_button("find profiles", lambda: self.create_popup_searching())
        self.create_button("Save credentials", self.set_cred)
        self.create_button("Connect", self.connect_command)

        self.dg = DataGoogle()


        return

    # Fonction creation des entry
    def create_entry(self, title, width, show, validatecommand, defaultText=''):
        self.create_label(title)
        if show:
            if validatecommand == ():
                entry = tk.Entry(self.root, width=width, text=defaultText)
            else:
                entry = tk.Entry(self.root, validate="key", validatecommand=validatecommand, width=width)
        else:
            if validatecommand == ():
                entry = tk.Entry(self.root, show='*', width=width, text=defaultText)
            else:
                entry = tk.Entry(self.root, show='*', validatecommand=validatecommand, validate="key", width=width, text=defaultText)
        self.list_manipulable_object[title] = entry
        entry.insert(0, defaultText)
        entry.pack()
        return entry

    # Fonction creation de boutons
    def create_button(self, text, function=''):
        button = tk.Button(self.root, text=text, command=function)
        button.pack()

    # Fonction creation de label (titre)
    def create_label(self, title):
        tk.Label(self.root, text=title, width=100).pack(expand=True)

    # Opens small window to confirm search
    def create_popup_searching(self):
        self.new_root2 = tk.Tk()
        search_label = tk.Label(self.root, text='Searching...', width=50)
        search_label.pack()
        tk.Label(self.new_root2, text='Search?', width=50).pack()
        tk.Button(self.new_root2, text='Yes', command=self.add_data_command).pack(expand=True)
        tk.Button(self.new_root2, text='No', command=lambda: (search_label.destroy(), self.new_root2.destroy())).pack(expand=True)

    # Creates box to show results in root
    def print_results(self, list_profiles):
        # Res="\n".join(list_profiles)
        df = pd.read_csv("submission_form_database.csv")
        if list_profiles == []:
            self.mylist.insert(tk.END, 'No result found')
        for profile in list_profiles:
            if type(df["open_to_work?_[yes/no]"].iloc[-1]) is not str:
                if int(profile["experience_duration"].split('an')[0]) >= df["minimum_experience_years"].iloc[-1]:
                    for i, j in profile.items():
                        if i == 'previous experiences':
                            self.mylist.insert(tk.END, i)
                            cnt = 1
                            for exp in j:
                                self.mylist.insert(tk.END, 'Experience ' + str(cnt) + " : ")
                                cnt += 1
                                for info, _ in exp.items():
                                    self.mylist.insert(tk.END, str(info) + " : " + str(_))
                        else:
                            self.mylist.insert(tk.END, str(i) + " : " + str(j))
            else:
                if int(profile["experience_duration"].split('an')[0]) >= df["minimum_experience_years"].iloc[-1] and profile[
                    "Open_to_work"].lower() == df["open_to_work?_[yes/no]"].iloc[-1].lower():
                    # or df["open_to_work?_[yes/no]"].iloc[-1].lower() == ''):
                    for i, j in profile.items():
                        if i == 'previous experiences':
                            self.mylist.insert(tk.END, i)
                            cnt = 1
                            for exp in j:
                                self.mylist.insert(tk.END, 'Experience ' + str(cnt) + " : ")
                                cnt += 1
                                for info, _ in exp.items():
                                    self.mylist.insert(tk.END, str(info) + " : " + str(_))
                        else:
                            self.mylist.insert(tk.END, str(i) + " : " + str(j))
            self.mylist.insert(tk.END, "***************************** \n *****************************")
        self.mylist.pack()
        self.scrollbar.config(command=self.mylist.yview)

    # Fonctions associées au bouton Add Data
    def add_data_command(self):
        if self.head.get():
            self.pa = ProfileAnalyser("detach")
        else:
            self.pa = ProfileAnalyser("--headless")
        self.new_root2.destroy()
        self.get_data_form()
        self.profiles = self.dg.find_links(self)
        self.profiles = self.pa.profile_analyser(self.profiles, self)
        self.pa.profiles2csv(self.profiles)
        self.pa.ask_open_excel()
        self.print_results(self.profiles)
        self.create_label('Search complete')

    # Fonction associée au bouton Connect
    def connect_command(self):
        self.pa.add_connection(self.profiles, self)
        self.create_label('Connection requests sent')

    # Fonction parsing des credentiels
    def get_default_cred(self):
        try:
            f = open('conf.json', 'r')
        except:
            try:
                return {"email": self.list_manipulable_object["email"].get(), "password": self.list_manipulable_object["password"].get()}
            except:
                return {"email": "", "password": ""}
        else:
            cred = json.load(f)
            return cred

    # Returns message to send with connection request
    def get_message(self):
        df = pd.read_csv("submission_form_database.csv")
        try:
            if math.isnan(float(df["send_a_message"].iloc[-1])):
                return self.list_manipulable_object["Send a message"].get() + " "
            else:
                return df["send_a_message"].iloc[-1] + " "
        except:
            return df["send_a_message"].iloc[-1] + " "

    # Fonction ecriture des credentiels par default
    def set_cred(self):
        cred = {
            "email": self.list_manipulable_object["email"].get(),
            "password": self.list_manipulable_object["password"].get()
        }
        if os.path.exists('conf.json'):
            # If the file exists, update its contents with the new data
            with open('conf.json', 'r') as f:
                existing_data = json.load(f)
            existing_data.update(cred)
            with open('conf.json', 'w') as f:
                json.dump(existing_data, f)
        else:
            # If the file does not exist, create it with the new data
            with open('conf.json', 'w') as f:
                json.dump(cred, f)

    # Function to add data to the dataframe
    def get_data_form(self):
        data = {}
        [data.update({attributes[0].lower().replace(' ', '_'): self.list_manipulable_object[attributes[0]].get()}) for
         attributes in
         self.list_entry]
        print(data)
        try:
            df = pd.read_csv("submission_form_database.csv")
        except:
            df = pd.DataFrame(data, index=[])
            df.to_csv("submission_form_database.csv", index_label=False)
            # df.update(data)
        else:
            df = df.append(data, ignore_index=True)
            df.to_csv("submission_form_database.csv", index_label=False)

    def run(self):
        self.root.mainloop()
        return


if __name__ == "__main__":
    Interface().run()
