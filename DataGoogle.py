import string
import pandas as pd
import yagooglesearch
import random
import math


class DataGoogle:
    def __init__(self):
        pass

    # Returns the requested number of links
    def get_request_amount(self, IF):
        try:
            df = pd.read_csv('submission_form_database.csv')
            if math.isnan(df["requests_amount_-_do_not_exceed_20"].iloc[-1]):
                return 10
            if df["requests_amount_-_do_not_exceed_20"].iloc[-1]>20:
                return 20
            return df["requests_amount_-_do_not_exceed_20"].iloc[-1]
        except:
            return 20

    # Returns the string to pass in google search bar
    def getQuery(self, IF):
        df = pd.read_csv('submission_form_database.csv')

        if type(df["key_words"].iloc[-1]) is not str:
            key_words = ''
        else:
            key_words = ' '.join([i for i in df["key_words"].iloc[-1].split(' ')])

        if type(df["must_have"].iloc[-1]) is not str:
            musts = ''
        else:
            musts = ' '.join(["\"" + i + "\"" for i in df["must_have"].iloc[-1].split(', ')])

        if type(df["exclude"].iloc[-1]) is not str:
            excludes = ''
        else:
            excludes = ' '.join(["-" + i for i in df["exclude"].iloc[-1].split(' ')])

        if IF.alea.get():
            query = "site:fr.Linkedin.com/in " + key_words + " " + musts + " " + excludes + " " + random.choice(string.ascii_lowercase + "1234567890")
        else:
            query = "site:fr.Linkedin.com/in " + key_words + " " + musts + " " + excludes

        print(query)
        return query

    # Returns a list of profiles links
    def find_links(self, IF):
        rq = self.get_request_amount(IF)
        client = yagooglesearch.SearchClient(
            self.getQuery(IF),
            tbs="li:1",
            max_search_result_urls_to_return=rq,
            http_429_cool_off_time_in_minutes=45,
            http_429_cool_off_factor=1.5,
            # proxy="socks5h://127.0.0.1:9050",
            verbosity=5,
            verbose_output=False,  # False (only URLs) or True (rank, title, description, and URL)
        )
        client.assign_random_user_agent()
        return client.search()


if __name__ == "__main":
    print(DataGoogle().find_links())
