from parsel import Selector
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import random
import re
import pandas as pd
import os
import tkinter as tk

class ProfileAnalyser:
    def __init__(self, options):
        self.chrome_options = Options()
        self.chrome_options.add_argument(options)
        self.driver = webdriver.Chrome(options=self.chrome_options)
        self.cnt = 0
        self.profile_summaries = []
        return

    def profile_opener(self, profile, IF):
        timer = random.uniform(15, 30)
        print(timer)
        time.sleep(timer)
        self.driver.execute_script("window.open('');")
        self.driver.switch_to.window(self.driver.window_handles[self.cnt])
        self.cnt += 1
        self.driver.get(profile)
        self.driver.implicitly_wait(10)
        try:
            button = self.driver.find_element(By.XPATH,
                                              "/html/body/div[1]/div/section/div/div[2]/button[2]")  # Trouver le bouton Refuser cookies
        except:
            print("Pas de bouton Refuser Cookies")
        else:
            self.driver.execute_script("arguments[0].click();", button)
            self.driver.implicitly_wait(10)

        try:
            button = self.driver.find_element(By.XPATH,
                                              "/html/body/div[2]/div/div/section/main/div/div/div[1]/button")  # Trouver le bouton s'identifier
        except:
            print("Pas de bouton S'identifier")
        else:
            print("On a trouvé le bouton S'identifier")
            self.driver.execute_script("arguments[0].click();", button)
            self.driver.implicitly_wait(10)
            try:
                email_entry = self.driver.find_element(By.XPATH,
                                                       "/html/body/div[2]/div/div/section/main/div/form[1]/div[1]/div[1]/div/div/input")  # Trouver le champ email
                pwd_entry = self.driver.find_element(By.XPATH,
                                                     "/html/body/div[2]/div/div/section/main/div/form[1]/div[1]/div[2]/div/div/input")  # Trouver le champ mot de passe
            except:
                print("No email entry")
            else:
                email = IF.get_default_cred()["email"]
                email_entry.send_keys(email)
                pwd = IF.get_default_cred()["password"]
                pwd_entry.send_keys(pwd)
                button = self.driver.find_element(By.XPATH,
                                                  "/html/body/div[2]/div/div/section/main/div/form[1]/div[2]/button")  # Trouver le bouton se connecter
                self.driver.execute_script("arguments[0].click();", button)
                self.driver.implicitly_wait(10)

        try:
            email_entry = self.driver.find_element(By.ID, "email-or-phone")  # Trouver le champ email
            pwd_entry = self.driver.find_element(By.XPATH,"/html/body/div/main/div/form/section/div[2]/div[2]/input")  # Trouver le champ mot de passe
        except:
            print("No email entry here")
        else:
            print("On a trouvé une entrée pour l'email")
            email = IF.get_default_cred()["email"]
            email_entry.send_keys(email)
            pwd = IF.get_default_cred()["password"]
            pwd_entry.send_keys(pwd)
            button = self.driver.find_element(By.XPATH,"/html/body/div/main/div/form/section/button")  # Trouver le bouton se connecter
            self.driver.execute_script("arguments[0].click();", button)
            self.driver.implicitly_wait(10)

    def profile_experience_duration(self, profiles):
        for profile_summary in profiles:
            previous_experiences = profile_summary["previous experiences"]
            exp_duration_years = 0
            exp_duration_months = 0
            for i in previous_experiences:
                try:
                    #Si le parsing est defectueux ou la duree n'est pas renseignee, remplacer par 0
                    if i["years"] == '':
                        exp_duration_months = exp_duration_years = 0
                    else:

                        if ' · ' in i["years"]:
                            exp_duration = i["years"].split(' · ')[1]
                        else:
                            exp_duration = i["years"]

                        if exp_duration == 'Moins d’un an':
                            exp_duration_years +=1
                        else:
                            if 'an' not in exp_duration:
                                exp_duration_months += int(exp_duration.replace('mois', ''))
                            else:
                                if 'ans' in i["years"]:
                                    exp_duration_years += int(exp_duration.split('ans')[0])
                                    if 'mois' in exp_duration.split('ans')[1]:
                                        exp_duration_months += int(exp_duration.split('ans')[1].replace('mois', ''))
                                else:
                                    exp_duration_years += int(exp_duration.split('an')[0])
                                    if 'mois' in exp_duration.split('an')[1]:
                                        exp_duration_months += int(exp_duration.split('an')[1].replace('mois', ''))
                except:
                    continue
            exp_duration_years += exp_duration_months // 12
            exp_duration_months = exp_duration_months % 12
            print(profile_summary["name"])
            print(exp_duration_years, 'ans', exp_duration_months, 'mois')
            if exp_duration_years > 1:
                profile_summary["experience_duration"] = str(exp_duration_years) + 'ans' + str(exp_duration_months) + 'mois'
            else:
                profile_summary["experience_duration"] = str(exp_duration_years) + 'an' + str(exp_duration_months) + 'mois'

    def profile_analyser(self, links, IF):
        self.profile_summaries = []
        for profile in links:
            self.profile_opener(profile, IF)

            profile_summary = {'url': profile}
            try:
                # Scroll to the bottom of the page to load more content
                experience_section = WebDriverWait(self.driver, 30).until(
                    EC.presence_of_element_located((By.ID, "experience")))

                self.driver.execute_script('arguments[0].scrollIntoView(true);', experience_section)
            except:
                pass

            # If the experience section is collapsed, expand it
            try:
                see_more_button = experience_section.find_element(By.XPATH,
                                                                  './/button[contains(@class, "pvs-navigation__text")]')
                self.driver.execute_script("arguments[0].click();", see_more_button)
                self.driver.implicitly_wait(10)
                WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                    (By.XPATH, './/button[contains(@class, "pv-profile-section__see-more-inline")]')))
            except:
                pass

            page = self.driver.page_source
            # print(page)

            self.sel = Selector(text=self.driver.page_source)

            # xpath to extract the text from the class containing the name
            name = re.sub(r'(?=<).*?(?=>).*', '',
                          self.sel.xpath('//*[contains(@class,"artdeco-entity-lockup__title")] / text()').extract_first().replace('é', 'e').replace('è', 'e').replace('à', 'a'))

            if name:
                name = name.strip()
                # print("Name:", name)
                profile_summary["name"] = name
            else:
                print("Pas de name")

            # xpath to extract the text from the class containing the job title
            job_title = re.sub(r'(?=<).*?(?=>).*', '',
                               self.sel.xpath('//*[contains(@class,"artdeco-entity-lockup__subtitle")] / text()').extract_first().replace('é','e').replace('è', 'e').replace('à', 'a'))

            if job_title:
                job_title = job_title.replace('amp;', '').replace('é', 'e').replace('è', 'e').replace('à', 'a').strip()
                # print("Job title:", job_title)
                profile_summary["job_title"] = job_title
            else:
                print("Pas de job title")

            # xpath to extract the text from the class containing the company
            company = re.sub(r'(?=<).*?(?=>).*', '',page.split('tabindex="-1">')[1].strip()[:page.split('tabindex="-1">')[1].strip().index('\n')].replace('é', 'e').replace('è', 'e').replace('à', 'a'))

            if company:
                company = company.strip().replace('é', 'e').replace('è', 'e').replace('à', 'a')
                # print("Company:", company)
                profile_summary["company"] = company
            else:
                print("Pas de company")

            # xpath to extract the text from the class containing the collegetext-body-medium break-words
            try:
                college = re.sub(r'(?=<).*?(?=>).*', '',page.split('tabindex="-1">')[2].strip()[:page.split('tabindex="-1">')[2].strip().index('\n')].replace('é','e').replace('è', 'e').replace('à', 'a'))
            except:
                # print("Pas de formation renseignée")
                profile_summary["college"] = ''
            else:
                # print("College:", college.strip())
                profile_summary["college"] = college

            # xpath to extract the text from the class containing the location
            location = re.sub(r'(?=<).*?(?=>).*', '', self.sel.xpath('//*[contains(@class,"text-body-small inline")] / text()').extract_first().replace('é', 'e').replace('è', 'e').replace('à', 'a'))

            if location:
                location = location.replace('é', 'e').replace('è', 'e').replace('à', 'a').strip()
                # print("Location:", location)
                profile_summary["location"] = location
            else:
                print("Pas de location")

            if 'open to work' in page.lower() or '#open_to_work' in page.lower():
                profile_summary["Open_to_work"] = 'Yes'
            else:
                profile_summary["Open_to_work"] = 'No'

            i=0
            for exp in page.split('<section'):
                if "id=\"experience\"" in exp:
                    i = page.split('<section').index(exp)
            if i == 0:
                profile_summary["previous experiences"] = "Experience section not filled in"
                print(profile_summary)
                continue

            experience_section = page.split('<section')[i].split('artdeco-list__item pvs-list__item--line-separated pvs-list__item--one-column')
            exp_list_dict = []
            exp_dict = {}
            for exp in experience_section[1:]:
                exp_dict = {}
                if 'pvs-entity__path-node' in exp:
                    new_exp = exp.split('display-flex flex-column full-width align-self-center')
                    exp_dict = {"company": re.sub(r'(?=<).*?(?=>).*', '',
                                                  new_exp[1].split('<!---->')[1].replace('é', 'e').replace('è', 'e').replace('à', 'a').strip()),
                                "years": re.sub(r'(?=<).*?(?=>).*', '',
                                                new_exp[1].split('<!---->')[8].replace('é', 'e').replace('è', 'e').replace('à', 'a').replace('&nbsp;', '').strip())}
                    for i in range(len(new_exp[2:])):
                        exp_dict["post" + str(i + 1)] = re.sub(r'(?=<).*?(?=>).*', '',new_exp[i + 2].split('<!---->')[1].strip().replace('amp;', '').replace('é','e').replace('è', 'e').replace('à', 'a'))
                    # print(exp_dict)
                    exp_list_dict += [exp_dict]
                    # print(exp_list_dict)
                elif exp.count('<!---->') == 24:
                    exp_dict = {
                        "post": re.sub(r'(?=<).*?(?=>).*', '', exp.split('<!---->')[3].strip().replace('amp;', '').replace('é', 'e').replace('è', 'e').replace('à', 'a')),
                        "company": re.sub(r'(?=<).*?(?=>).*', '',exp.split('<!---->')[10].strip().replace('é', 'e').replace('è', 'e').replace('à', 'a')),
                        "years": re.sub(r'(?=<).*?(?=>).*', '',exp.split('<!---->')[14].replace('&nbsp;', '').strip().replace('é', 'e').replace('è','e').replace('à', 'a')),
                        "region": re.sub(r'(?=<).*?(?=>).*', '',exp.split('<!---->')[18].strip().replace('é', 'e').replace('è', 'e').replace('à', 'a'))
                    }
                    exp_list_dict += [exp_dict]
                else:
                    exp_dict = {
                        "post": re.sub(r'(?=<).*?(?=>).*', '', exp.split('<!---->')[4].strip().replace('amp;', '').replace('é', 'e').replace('è', 'e').replace('à', 'a')),
                        "company": re.sub(r'(?=<).*?(?=>).*', '',exp.split('<!---->')[11].strip().replace('é', 'e').replace('è', 'e').replace('à', 'a')),
                        "years": re.sub(r'(?=<).*?(?=>).*', '',exp.split('<!---->')[15].replace('&nbsp;', '').strip().replace('é', 'e').replace('è','e').replace('à', 'a')),
                        "region": re.sub(r'(?=<).*?(?=>).*', '',exp.split('<!---->')[19].strip().replace('é', 'e').replace('è', 'e').replace('à', 'a'))
                    }
                    exp_list_dict += [exp_dict]
                profile_summary["previous experiences"] = exp_list_dict
            print(profile_summary)
            self.profile_summaries += [profile_summary]
        self.profile_experience_duration(self.profile_summaries)
        return self.profile_summaries

    def profiles2csv(self, profiles):
        try:
            df = pd.read_csv("profile_summaries.csv")
        except:
            # print('We had to make csv')
            df = pd.DataFrame(profiles)
            df.to_csv("profile_summaries.csv", index=False)
        else:
            # print('We found csv')
            for profile in profiles:
                # print(profile)
                df = df.append(profile, ignore_index=True)
                df.to_csv("profile_summaries.csv", index=False)

    def add_connection(self, profiles, IF):
        for profile in profiles:
            self.profile_opener(profile["url"], IF)
            page = self.driver.page_source

            #Verifier si le bouton se connecter est affiche
            if 'Se connecter' in page.split("<span class=\"artdeco-button__text\">")[5].split('\n')[1].strip():
                button = self.driver.find_element(By.ID, self.driver.page_source.split("à rejoindre votre réseau\" id=\"")[2].split('"')[0])
                self.driver.execute_script("arguments[0].click();", button)
                self.driver.implicitly_wait(10)

                button = self.driver.find_element(By.XPATH, "/html/body/div[3]/div/div/div[3]/button[1]/span")
                self.driver.execute_script("arguments[0].click();", button)
                self.driver.implicitly_wait(10)

                message_entry = self.driver.find_element(By.XPATH, "/html/body/div[3]/div/div/div[2]/div/textarea")
                message = IF.get_message()
                message_entry.send_keys(message)
                button = self.driver.find_element(By.XPATH,
                                                  "/html/body/div[3]/div/div/div[3]/button[2]/span")
                self.driver.execute_script("arguments[0].click();", button)
                self.driver.implicitly_wait(10)

            #Dans le cas ou le bouton suivre est affiche
            if 'Suivre' in page.split("<span class=\"artdeco-button__text\">")[5].split('\n')[1].strip():
                button = self.driver.find_element(By.ID, self.driver.page_source.split("aria-label=\"Plus d’actions\" id=\"")[1].split(
                    "\" class=\"artdeco-dropdown__trigger")[0])
                self.driver.execute_script("arguments[0].click();", button)
                self.driver.implicitly_wait(10)

                button = self.driver.find_element(By.ID, page.split('Se connecter')[0].split('id="')[-1].split('" class="')[0])
                self.driver.execute_script("arguments[0].click();", button)
                self.driver.implicitly_wait(10)

                button = self.driver.find_element(By.XPATH, "/html/body/div[3]/div/div/div[3]/button[1]/span")
                self.driver.execute_script("arguments[0].click();", button)
                self.driver.implicitly_wait(10)

                message_entry = self.driver.find_element(By.XPATH, "/html/body/div[3]/div/div/div[2]/div/textarea")
                message = IF.get_message()
                message_entry.send_keys(message)
                button = self.driver.find_element(By.XPATH,
                                                  "/html/body/div[3]/div/div/div[3]/button[2]/span")
                self.driver.execute_script("arguments[0].click();", button)
                self.driver.implicitly_wait(10)

    def open_excel(self):
        try:
            df = pd.read_csv('profile_summaries.csv')

            # Convert the dataframe into an Excel table and save it to a new Excel file
            with pd.ExcelWriter("profile_summaries.xlsx") as writer:
                df.to_excel(writer, index=False)

            os.system(f'start excel "profile_summaries.xlsx"')
            self.new_root.destroy()
        except:
            print("No data to parse for the moment")

    def ask_open_excel(self):
        self.new_root = tk.Tk()
        tk.Label(self.new_root, text='Excel generated. Do you want to open it?', width=50).pack()
        tk.Button(self.new_root, text='Yes', command=self.open_excel).pack()
        tk.Button(self.new_root, text='No', command=self.new_root.destroy).pack()


if __name__ == "__main__":
    from Interface import Interface
    pa = ProfileAnalyser("detach")
    IF = Interface()
    pa.profile_analyser([
        # 'https://www.linkedin.com/in/oulad-amara/?originalSubdomain=fr',
        # 'https://www.linkedin.com/in/olivier-germani/',
        'https://fr.linkedin.com/in/jessica-delacourt-35508144',
    ], IF)
